* KiMidi
The energy of midi

** introduction
Is a software midi controller

** features
- fully configurable boxes of knobs and sliders (midi control change (CC)) (you can choose even the colors!)
- sends keyboards events (midi ON and midi OFF) (key map shared/like vkeybd)
- several modes available to respond to events, in emacs-style fashion
- debug mode

** dependencies
- python3.6+
- a midi backend for mido (such as rtmidi (I'm developing with python-rtmidi))
- see setup.py for all the dependencies

** installation

*** instructions for developers (or just to get the latest version)
1. git clone this repository
2. *cd* into the root of the project
3. create a virtualenv and activate that (I suggest virtualenvwrapper but you can just *virtualenv venv; source venv/bin/activate*)
4. install the dependecies with *pip install -e ".[dev]"*
5. install the mido backend of your choice, eg *pip install python-rtmidi* (this is platform dependent so it is up to you)
6. run it with *python main.py*, enjoy

** example with extempore
run kimidi, then
run extempore with *--midiin="RtMidi"* and evaluate the following code

#+BEGIN_SRC extempore
(sys:load "examples/sharedsystem/setup.xtm")

(bind-func bare_start_note
  (lambda (inst:[SAMPLE,SAMPLE,i64,i64,SAMPLE*]* pitch:i32 volume:i32)
    (let ((dargs:SAMPLE* (alloc)))
      (xtm_start_note (now) inst (midi2frq (convert pitch float))
		      (midi2frq (convert volume float)) 0 dargs))))

(bind-func midi_note_on:[void,i32,i32,i32,i32]*
  (lambda (timestamp:i32 pitch:i32 value:i32 chan:i32)
    (println "got note on" pitch value chan)
    (bare_start_note syn1 pitch value)
    void))

(bind-func get_stopping_note:[NoteData*,[SAMPLE,SAMPLE,i64,i64,SAMPLE*]*,i32]*
  (lambda (inst:[SAMPLE,SAMPLE,i64,i64,SAMPLE*]* pitch:i32)
    (let ((note:NoteData* null)
	  (loop (lambda (i:i64)
		  (println i pitch (frq2midi (note_frequency (pref (inst.notes:NoteData**) i))))
		  (if (= (convert pitch float)
			 (frq2midi (note_frequency (pref (inst.notes:NoteData**) i))))
		      (set! note (pref (inst.notes:NoteData**) i))
		      (if (< i (- MAXPOLYPHONY 1))
			  (loop (+ i 1))
			  null)))))
      (loop 0)
      (println "stopping note" note)
      note)))

(bind-func midi_note_off:[void,i32,i32,i32,i32]*
  (lambda (timestamp:i32 pitch:i32 volume:i32 chan:i32)
    (println "note off" timestamp pitch volume chan)
    (let ((stopping_note:NoteData* (get_stopping_note syn1 pitch)))
      (println "stopping now" pitch stopping_note)
      (xtm_stop_note (cast stopping_note))
      void)))
#+END_SRC
Then 
This example shows how to use kimidi with extempore sharedsystem example
using only one analogue synth *syn1* you can use the chan parameter to bind other instruments


** contributing
set git hooks to run flake8 before commit
`git config core.hooksPath .githooks/`
